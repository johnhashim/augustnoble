<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package august noble
 */

?>

</div><!-- #content -->
<section class="grid-container  contact-button-section" style="background-color:<?php the_field( 'background_color', 'option' ); ?>">
	<div class="grid-x">
	<h2 class="results-information"><?php the_field( 'header_text', 'option' ); ?></h2>
	<h4 class="cta-text"><?php the_field( 'subtitle_text', 'option' ); ?></h4>
	<div class="the-subscribe-form">
	<?php
		$form = get_field( 'subscribe_form', 'option' );
		gravity_form( $form, false, true, false, '', true, 1 );
	?>
 </div>
	</div><!-- .grid-x -->
</section><!-- .cta-block -->

	<footer class="site-footer">
		<div class="grid-x">
			<nav id="site-navigation" class="footer-navigation" aria-label="<?php esc_html_e( 'Footer Menu', 'augustnoble' ); ?>">
						<?php
						wp_nav_menu( array(
							'fallback_cb'     => false,
							'theme_location'  => 'footer',
							'menu_id'         => 'footer-menu',
							'menu_class'      => 'menu dropdown',
							'container'       => false,
							'container_class' => '',
							'container_id'    => '',
							'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
						) );
						?>
					</nav>

			<div class="site-info">
				<div class="footer-logo">
				 <?php the_custom_logo(); ?>
				</div>
				<?php august_noble_display_copyright_text(); ?>
				<?php august_noble_display_social_network_links(); ?>
			</div><!-- .site-info -->
		</div>
	</footer><!-- .site-footer container-->
</div><!-- #page -->

<?php wp_footer(); ?>

<?php august_noble_display_mobile_menu(); ?>

</body>
</html>
