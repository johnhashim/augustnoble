<?php
/**
 *  The template used for displaying fifty/fifty media/text.
 *
 * @package august noble
 */

// Set up fields.
$animation_class = august_noble_get_animation_class();

// Start a <container> with a possible media background.
august_noble_display_block_options( array(
	'container' => 'section', // Any HTML5 container: section, div, etc...
	'class'     => 'grid-container testimonials', // The class of the container.
) );
?>
	<div class="cleints-testimony grid-x<?php echo esc_attr( $animation_class ); ?>">

<?php

if ( have_rows( 'clients_testimonials' ) ) :

// Loop through the rows of data.
while ( have_rows( 'clients_testimonials' ) ) :
the_row();

// Display a sub field value.
if ( get_row_index() % 2 == 0 ) {
?>
<div class="testimonial">
<h2 class="name name-even"><?php the_sub_field( 'clinent_name' ); ?></h2>
<h4 class=" bussiness-info bussiness-info-even"><?php the_sub_field( 'company_position' ); ?><a href="<?php echo esc_url( $company_url ); ?>"><?php the_sub_field( 'company_name' ); ?></a></h4>
<div class="review even">
<div class="client-img">
<img class="fifty-image" src="<?php the_sub_field( 'client_image' ); ?>" alt="<?php the_sub_field( 'client_image' ); ?>">
</div>
<div class="testimony">
<?php the_sub_field( 'testimonial' ); ?>
</div>
</div>
</div>

<?php
} else {
 ?>

<div class="testimonial">
<h2 class="name"><?php the_sub_field( 'clinent_name' ); ?></h2>
<h4 class="bussiness-info"><?php the_sub_field( 'company_position' ); ?><a href="<?php echo esc_url( $company_url ); ?>"><?php the_sub_field( 'company_name' ); ?></a></h4>
<div class="grid-x review odd">
<div class="client-img">
<img class="fifty-image" src="<?php the_sub_field( 'client_image' ); ?>" alt="<?php the_sub_field( 'client_image' ); ?>">
</div>
<div class="testimony">
<?php the_sub_field( 'testimonial' ); ?>
</div>
</div>
</div>
<?php

}
endwhile;
endif;
?>

	</div><!-- .grid-x -->
</section><!-- .fifty-media-text -->
