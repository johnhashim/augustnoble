<?php
/**
 * The template used for displaying a CTA block.
 *
 * @package august noble
 */

// Set up fields.
$title         = get_sub_field( 'section_header' );
$result           = get_sub_field( 'results_information' );
$text            = get_sub_field( 'results_footer_text' );
$animation_class = august_noble_get_animation_class();

// Start a <container> with possible block options.
august_noble_display_block_options(
	array(
		'container' => 'section', // Any HTML5 container: section, div, etc...
		'class'     => 'grid-container results', // Container class.
	)
);
?>
<div class="grid-wrap grid-x<?php echo esc_attr( $animation_class ); ?>">

<div class="inner-wrap">
<h1 class="section-header"><?php echo esc_html( $title ); ?></h1>
<?php

// Check if the repeater field has rows of data.
if ( have_rows( 'results' ) ) :

// Loop through the rows of data.
while ( have_rows( 'results' ) ) :
the_row();

 if ( $title ) :
 ?>
<h4 class="results-information"><?php the_sub_field( 'results_information' ); ?></h4>
<?php
endif;
endwhile;
endif;

?>

<?php if ( $text ) : ?>
<h4 class="cta-text"><?php echo esc_html( $text ); ?></h4>
<?php endif; ?>
</div>
</div><!-- .grid-x -->
</section><!-- .cta-block -->
