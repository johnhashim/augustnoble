<?php
/**
 * The template used for displaying a CTA block.
 *
 * @package august noble
 */

// Set up fields.
$title           = get_sub_field( 'header_title' );
$text            = get_sub_field( 'summary' );
$animation_class = august_noble_get_animation_class();

// Start a <container> with possible block options.
august_noble_display_block_options(
	array(
		'container' => 'section', // Any HTML5 container: section, div, etc...
		'class'     => 'grid-container page-header', // Container class.
	)
);
?>
<div class="grid-wrap grid-x<?php echo esc_attr( $animation_class ); ?>">
<div class="outer-wrap">
<div class="header-wrap">
<?php if ( $title ) : ?>
<h1 class="cta-title"><?php the_sub_field( 'header_title' ); ?></h1>
<?php endif; ?>

<?php if ( $text ) : ?>
<h4 class="cta-text"><?php echo esc_html( $text ); ?></h4>
<?php endif; ?>
</div>
</div>
</div><!-- .grid-x -->
</section><!-- .cta-block -->
