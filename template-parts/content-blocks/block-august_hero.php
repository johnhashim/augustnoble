<?php
/**
 *  The template used for displaying fifty/fifty text/media.
 *
 * @package august noble
 */

// Set up fields.
$header          = get_sub_field( 'header' );
$subtitle        = get_sub_field( 'subtitle_header' );
$tagline       = get_sub_field( 'page_tagline' );
$image_data      = get_sub_field( 'profile_image' );
$button_text        = get_sub_field( 'contact_button' );
$button_link        = get_sub_field( 'contact_link' );
$summary            = get_sub_field( 'summary' );

$animation_class = august_noble_get_animation_class();

// Start a <container> with a possible media background.
august_noble_display_block_options( array(
	'container' => 'section', // Any HTML5 container: section, div, etc...
	'class'     => ' grid-container  august-hero', // Container class.
) );
?>
	<div class=" profile grid-x<?php echo esc_attr( $animation_class ); ?>">
	<div class=" page-header">
		<div class=" page-header-inner">
			<div class="heading">
				<h1 class="page-title"> <?php echo esc_html( $header ); ?></h1>
				<h3 class="page-subtitle"> <?php echo esc_html( $subtitle ); ?> </h3>
			</div>
			<div class="page-tagline"><?php echo force_balance_tags( $tagline ); // WPCS: XSS OK. ?></div>
		</div>
		<a class="button get-in-touch red" href="<?php echo esc_url( $button_link ); ?>"><?php echo esc_html( $button_text ); ?></a>
	</div>
	<div class="covering-image">
		<img class="august-media-image" src="<?php echo esc_url( $image_data['url'] ); ?>" alt="<?php echo esc_html( $image_data['alt'] ); ?>">
	</div>

	</div><!-- .grid-x -->
</section><!-- .fifty-text-media -->
