<?php
/**
 *  The template used for displaying fifty/fifty text/media.
 *
 * @package august noble
 */

// Set up fields.
$animation_class = august_noble_get_animation_class();

// Start a <container> with a possible media background.
august_noble_display_block_options( array(
	'container' => 'section', // Any HTML5 container: section, div, etc...
	'class'     => 'grid-container clients', // Container class.
) );
?>
	<div class=" clients-img grid-x<?php echo esc_attr( $animation_class ); ?>">
<?php

if ( have_rows( 'clients_logo' ) ) :

// Loop through the rows of data.
while ( have_rows( 'clients_logo' ) ) :
the_row();

// Display a sub field value.
?>
<div class="logo">
<img class="covering-image" src="<?php the_sub_field( 'image' ); ?>" alt="<?php the_sub_field( 'image' ); ?>">
</div>
<?php
endwhile;
endif;
?>
		
</div><!-- .grid-x -->
</section><!-- .fifty-text-media -->
